<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
header('Content-type: text/html; charset=UTF-8');


class SimpleClass{
    public $now = '';
    
    function __construct() {
        $this->now = date("Y-m-d"); 
    }
   
    public function display_now() {
        echo $this->now;
    }
}

$b = new SimpleClass();
$b->display_now();